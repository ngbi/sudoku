package graphique;

public class Affichage {

static final char EMPTY = '@';
	
	public static void main(String[] args) {
		char[][] grid = new char[][]{
			{ EMPTY, '9', '3',   EMPTY, '2', EMPTY,   EMPTY, EMPTY, '8' },
            { '7', '2', '4',   '6', EMPTY, '9',   EMPTY, '3', EMPTY },
            { EMPTY, '6', EMPTY,   EMPTY, EMPTY, '3',   EMPTY, EMPTY, '2' },
            { EMPTY, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY,   EMPTY, '2', '1' },
            { EMPTY, EMPTY, EMPTY,   '8', EMPTY, '4',   EMPTY, EMPTY, EMPTY },
            { '5', '7', EMPTY,   EMPTY, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY },
            { '9', EMPTY, EMPTY,   '3', EMPTY, EMPTY,   EMPTY, '8', EMPTY },
            { EMPTY, '3', EMPTY,   '7', EMPTY, '2',   '5', '1', '4' },
            { '2', EMPTY, EMPTY,   EMPTY, '5', EMPTY,   '6', '9', EMPTY } }; //on initialise la grille grid
		
		displayGrid( grid ); //affichage de la grille grid
		
		
	}
	
	
	public static void displayGrid(char[][] tab ) {
		
	
			System.out.println("---        Partie sudoku           ----");
		System.out.println("------------------------------");
		System.out.println("");
		for( short k = 0 ; k < tab.length ; k++) {
			for( short j = 0 ; j < tab[k].length ; j++) {
					
						System.out.print(tab[k][j] + " .");
					}
		
			System.out.println(" ");
		}
		System.out.println("------------------------------");
	}
}